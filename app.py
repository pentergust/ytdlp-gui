import customtkinter
from yt_dlp import YoutubeDL


class App(customtkinter.CTk):
    def __init__(self):
        super().__init__()

        self.title("yt-dlp loader")
        self.geometry("640x480")
        self.grid_columnconfigure((0, 0), weight=1)

        self.entry = customtkinter.CTkEntry(self, placeholder_text="Load url")
        self.entry.grid(row=0, column=0, padx=20, pady=20, sticky="nsew")

        self.button = customtkinter.CTkButton(self,
            text="Load", command=self.load_video
        )
        self.button.grid(row=1, column=0, padx=20, pady=20)

    def load_video(self):
        with YoutubeDL() as ydl:
            ydl.download([self.entry.get()])


if __name__ == "__main__":
    app = App()
    app.mainloop()